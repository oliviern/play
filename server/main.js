import { Meteor } from 'meteor/meteor';

Players = new Mongo.Collection("players")
Bonus = new Mongo.Collection("bonus")
PlayerBonus = new Mongo.Collection("playerbonus")
PlayerBonusActive = new Mongo.Collection("playerbonusactive")
Background = new Mongo.Collection("background");
Params = new Mongo.Collection("params");
Coins = new Mongo.Collection("coins");
Musics = new Mongo.Collection("musics");

var suggestions = {
    	lieu: ["Maison abandonnée", "Sas de décompression", "Porte d'embarquement", "Ambulance", "Magasin d'antiquités", "Fusée", "Apple Store", "Salle de jeux d'arcade", "Camp de base arctique", "Studio d'artiste", "Grenier", "Garçonnière", "Balcon", "Piscine à boules", "Banque", "Coffre-fort", "Sous-sol", "Repaire de superhéros", "Plage", "Chambre", "Rack à vélos", "Magasin de vélos", "Salle de billard", "Sanctuaire d'oiseaux", "Quai", "Jardin botanique", "Allée de bowling", "Ring de boxe", "Brasserie", "Magasin en gros", "Bus", "Arrêt de bus", "Boucherie", "Café", "Terrain de camping", "Confiserie", "Exposition d'automobiles", "Grotte", "Laboratoire CERN", "Cabine d'essayage", "Laboratoire de chimie", "Club d'échecs", "Salle de classe", "Cabine de pilotage", "Chambre froide", "Salle de tribunal", "Microbrasserie", "Salle de danse", "Ruelle sombre", "Dépanneur", "Dortoir", "Cabinet de médecin", "Allée de garage", "Pub irlandais", "Conseil d'administration", "Bunker", "Restaurant 5 étoiles", "Studio de mode", "Marché aux puces", "Fleuriste", "Camion-restaurant", "Magasin de jeux vidéo", "Garage", "Terrain de golf", "Serre de culture", "Salon de coiffure", "Grange", "Casiers d'école", "Calèche", "Stand de hot dog", "Jacuzzi", "Hall d'hôtel", "Toit d'une maison", "Centrale électrique", "Cabane de pêche", "Cellule de prison", "Bijouterie", "Chenil", "Table des enfants", "Cuisine", "Toilettes de femmes", "Cabinet d'avocat", "Bibliothèque", "Canot de sauvetage", "Limousine", "Magasin d'alcool", "Grotte", "Appartement à Manhattan", "Toilettes pour hommes", "Tente militaire", "Minivan", "Monastère", "Morgue", "Plateau de tournage", "Camion de déménagement", "Salle de musée", "Entrepôt d'un musée", "Observatoire", "Salle d'opération", "Bureau Oval", "Appartement-terrasse", "Photomaton", "Cour de récréation", "Salle de poker", "Jet privé", "Salle de projection", "Piscine publique", "Sables mouvants", "Studio de radio", "Studio d'enregistrement", "Ferme sur les toits", "Barque", "Saloon", "Sauna", "Plage isolée", "Donjon SM", "Conteneur", "Magasin de chaussures", "Bar de rencontres", "Skate Park", "Chalet de ski", "Squat", "Étable", "Loges de star", "Espace de stockage", "Wagon de métro", "Atelier clandestin", "Chez le tailleur", "Salon de thé", "Salle des professeurs", "Court de tennis", "Terrasse", "Salle du Trône", "Châlet", "Chambre de torture", "Caravane", "Vernissage", "Boutique vintage", "Carré VIP", "Salle d'attente", "Garde-robe", "Salle de musculation", "Bateau de rafting", "Cave à vins", "Atelier", "Tranchée", "Salle de yoga", "Sous un pont", "Temple", "Gare", "Dojo", "Château-fort", "Manoir", "Chambre d'hôtel", "Vaisseau spatial", "Salle d'urgence", "Mairie", "Kiosque d'information"],
    	relation: ["Pilote de vaisseau/Intelligence artificielle", "Passagers d'un avion", "Ambassadeur/Roi", "Archiviste/Chercheur", "Trafiquant d'arme/Parent", "Astronaute/Chef de mission", "Astronome/Météorologiste", "Auteur/Éditeur", "Bébé/Animal", "Babysitter/Nouveau parent", "Barman/Client régulier", "Barman/Serveur", "Ours/Ourson", "Forgeron/Cavalier", "Marié/Organisateur de mariage", "Chauffeur de bus/Client régulier", "Caissier/Client", "Naufragé/Noix de coco", "Chef/Maître d'hôtel", "Coach/Arbitre", "Chef d'orchestre/1er violon", "Cuisinier/Sous-Chef", "Cycliste/Conducteur", "Dentiste/Patient", "Dictateur/Général", "Docteur/Assassin", "Chef chat/Chef chien", "Ambulancier/Policier", "Entrepreneur/Banquier", "Cadre/Assistant", "Fermier/Épouvantail", "Capitaine de bateau/Second", "Contremaître/Architecte", "Garde forestier/Campeur", "Chef mafieux/Homme de main", "Famille d’accueil/Ado", "Jardinier/Garçon de piscine", "Coiffeur/Client régulier", "Clochard/Vendeur de rue", "Otage/Cambrioleur", "Cavalier/Vétérinaire", "Journaliste/Informateur", "Enfant/Parent", "Paysagiste/Propriétaire de maison", "Chauffeur/Conjoint trophée", "Bûcheron/Garde forestier", "Maire/Concierge", "Sage-femme/Femme enceinte", "Millionnaire/Vendeur de bateaux", "Conservateur de musée/Politicien", "Jeunes mariés", "Infirmière/Convalescent", "Optométriste/Patient", "Parent/Grand-parent", "Fêtard/Policier", "Personne/Son futur soi", "Toiletteur/Propriétaire d'animal", "Pharmacien/Client", "Photographe/Sujet", "Préparateur physique/Acteur", "Physicien/Mathématicien", "Accordeur de piano/Designer", "Pilote/Tour de contrôle", "Lanceur/Receveur", "Facteur/Livreur UPS", "Prince(sse)/Palefrenier", "Directeur/Enseignant", "Producteur/Artiste", "Professeur/Surdoué", "Programmeur/Étudiant", "Collectionneurs de livres", "Explorateurs rivaux", "Robot/Son inventeur", "Marin/Gardien de phare", "Chauffeur de bus/Dernier écolier", "Scénariste/Acteur", "Sculpteur/Modèle", "Vigile/Concierge", "Sénateur/Lobbyistes", "Vendeur de Skateboard/Ado", "Célébrité/Plombier", "Conjoint/Décorateur", "Conjoint/Beau-parent", "Espion(ne)/Entraîneur", "Délinquant/Policier en civil", "Étudiant/Parent riche", "Cascadeur/Réalisateur", "Criminel/Garde de prison", "Tailleur/Cordonnier", "Tatoueur/Perceur", "Chauffeur de taxi/Passager", "Propriétaire d'équipe/Superstar", "Ado/Grand-parent", "Adolescent/Parent", "Adolescent/Mentor", "Operateur téléphonique/Client", "Contrôleur de billets/Voyageur", "Bourreau/Bouffon", "Voyageur/Traducteur", "Deux Juges", "Deux designers de chaussures", "Représentant syndical/PDG", "Vampire/Loup-garou", "Horloger/Apprenti", "Bûcheron/Campeur", "Technicien rayons X/Docteur", "Beaux-frères ou soeurs", "Psy/Ex-patient", "Amoureux transis", "Touriste/Guide", "Adulte/Ami imaginaire", "Auteur connu/Admirateur"],
    	mot: ["Abandonné", "Académie", "Accent", "Accepté", "Acteur", "Dépendance", "Aventure", "Agent", "Aéroport", "Aliéné", "Seul", "Émerveillé", "Amusé", "Colère", "Anonyme", "Hymne", "Antiquités", "Apathie", "Architecte", "Dispute", "Aristocrate", "Armure", "Armée", "Arrivée", "Flèche", "Artiste", "Honteux", "Cendres", "Astrologie", "Atome", "Attaque", "Auditorium", "Répugnance", "Bar", "Barman", "Base-ball", "Batman", "Chauve-souris", "Plage", "Haricots", "Lit", "Entreprise", "Bimbo", "Biosphère", "Naissance", "Anniversaire", "Amertume", "Forgeron", "Blond", "Sang", "Empreinte", "Bombe", "Botte", "Ennuyé", "Emprunter", "Boston", "Boxeur", "Caleçon", "Salle de repos", "Rupture", "Mallette", "Slip", "Tyran", "Âne", "Cafétéria", "Gâteau", "Camping", "Canyon", "Capital", "Cartes", "Soins", "Carrière", "Cartographie", "Casino", "Cimetière", "PDG", "Président", "Chaise", "Charité", "Fromage", "Chimie", "Enfants", "Chine", "Chocolat", "Noël", "Église", "Cercle", "Guerre civile", "Falaise", "Nuages", "Club", "Cocktail", "Code", "Café", "Collector", "Comète", "Bandes dessinées", "Commerce", "Cône", "Confession", "Confiant", "Constitution", "Contrat", "Cuisinier", "Canapé", "Pays", "Courageux", "Tribunal", "Cowboy", "Vache", "Cricket", "Critique", "Foule", "Écraser", "Da Vinci", "Sombre", "Rendez-vous", "Débat", "Susceptible", "Abattu", "Démocratie", "Dépression", "Détective", "Détecteur", "Détestable", "Dévasté", "Diamant", "Directeur", "Désaccord", "Disco", "Déguisement", "Vaisselle", "Désillusionné", "Consterné", "Méprisé", "Distant", "Nunuche", "Diva", "Plonger", "Divorce", "Porte", "Dragon", "Drogue", "Donjon", "Poussière", "Enthousiaste", "Éclipse", "Éjecté", "Élections", "Ascenseur", "Éloquence", "Embarassé", "Vide", "Chiffrement", "Énergique", "Fiancé", "Enragé", "Urgence", "Escalator", "Exécution", "Bourreau", "Exercice", "Développer", "Expédition", "Expérience", "Explorer", "Juste", "Automne", "Gloire", "Ferme", "Combattant", "Licencié", "Musculation", "Marché aux puces", "Foie gras", "Falsification", "Fontaine", "Amis", "Effrayé", "Accompli", "Futur", "Oies", "Génie", "Authentique", "Géographie", "Fantôme", "Géant", "Abandon", "Lunettes", "But", "Chèvre", "Gouvernement", "Grammy", "Grèce", "Garde", "Guillotine", "Coupable", "Pirate", "Jambon", "Pendaison", "Has Been", "Chapeau", "Hanté", "Santé", "Coeur", "Héros", "Hésitant", "Dissimuler", "Hobby", "Itinérant", "Hockey", "Saint", "Accueil", "Optimiste", "Fer à cheval", "Otage", "Hostile", "Hôtel", "Maison", "Faim", "Ouragan", "Mari", "Bousculer", "Crème glacée", "Iceberg", "Ignoré", "Important", "Inadéquat", "Infâme", "Inférieur", "Infiltrer", "Furieux", "Innocence", "Curieux", "Précaire", "Insignifiant", "Inspiré", "Intérêt", "Internet", "Entrevue", "Intime", "Irrité", "Isolé", "Japon", "Jaloux", "Décalage horaire", "Jetpack", "Emploi", "Cavalier", "Journalisme", "Lutte", "Juge", "Jugement", "Jury", "Clé", "Cuisine", "Chaton", "Laser", "Lessive", "Paresse", "Départ", "Citron", "Laitue", "Libéré", "Littérature", "Prêt", "Haine", "Médaillon", "Logique", "Logo", "Londres", "Amour", "Amant", "Affectueux", "Chanceux", "Luxe", "Courrier", "Centre commercial", "Manières", "Sirop d'érable", "Plans", "Marche", "Masque", "Mathématiques", "Matelas", "Mediéval", "Mental", "Immigrant", "Lait", "Smoothie", "Ministre", "Monument", "Lune", "Film", "Déplacement", "Meurtre", "Nature", "Négociation", "Geek", "Journaux", "Insensé", "Carnet", "Serment", "Avoine", "Jeux Olympiques", "Ouvert", "Oscar", "Hors-la-loi", "Lendemain", "Envahi", "Emballage", "Pacte", "Peinture", "Palais", "Boite de Pandore", "Papier", "Paradoxe", "Parent", "Morceau", "Mot de passe", "Patriote", "Salaire", "Beurre d'arachide", "Paysan", "Pectoraux", "Penalité", "Stylo", "Pentagone", "Point", "Perplexe", "Physique", "Tarte", "Planète", "Platonique", "Joueur", "Jouer", "Pôle", "Politique", "Étang", "Piscine", "Marsouin", "Bureau de poste", "Pauvreté", "Puissant", "Impuissant", "Grossesse", "Préjudice", "Présent", "Présentation", "Président", "Fierté", "Premier ministre", "Prince", "Princesse", "Prison", "Programmation", "Propriété", "Fierté", "Provocant", "Provoqué", "Psychologie", "VTT", "Quête", "Question", "Démission", "Radio", "Pluie", "Rap", "Récréation", "Arbitre", "Réfraction", "Reggae", "Régime", "Archives", "Rejeté", "Remix", "Remords", "Répugnant", "Irrité", "Résident", "Respecté", "Revanche", "Révolté", "Révolution", "Dégoût", "Énigme", "Ridicule", "Alliance", "Rivalité", "Rivière", "Vedette", "Rome", "Rorschach", "Vendeur", "Salon", "Samouraï", "Sarcastique", "Économies", "Science-Fiction", "Sculpteur", "Séduction", "Vendu", "Sensible", "Requin", "Shérif", "Changeant", "Choqué", "Petit", "Pelle", "Épreuve", "Douche", "Fratrie", "Argent", "Abdominaux", "Sceptique", "Sommeil", "Sangles", "Fumée", "Fumer", "Savon", "Soccer", "Rupture de stock", "Supersonique", "Espace", "Vaisseau", "Discours", "Sphère", "Printemps", "Espion", "Carré", "Stade", "Taches", "Statue", "Stérile", "Bourse", "Orage", "Studio", "Soustraction", "Sucre", "Été", "Coup de soleil", "Coucher de soleil", "Chirurgie", "Surprise", "Méfiant", "Épée", "Table", "Grand", "Impôts", "Taxonomie", "Thé", "Adolescent", "Télégramme", "Tempérament", "Apéritif", "Soleil", "Théâtre", "Billets", "Chatouilles", "Retour en arrière", "Fuseau horaire", "Pourboire", "Titanesque", "Tabac", "Top secret", "Tortue", "Serviette", "Tracteur", "Échanger", "Trafic", "Piégé", "Saccagé", "Traumatisme", "Trésor", "Traitement", "Agriculture", "Halloween", "Trophée", "Confiance", "Télévision", "Jumeaux", "Demi-tour", "Arbitre", "Injuste", "Uniforme", "Union", "Malchance", "Déséquilibré ", "Majuscule", "Courrier recommandé", "Aspirateur", "Vampire", "Vanille", "Voûte", "Versailles", "Jeux vidéo", "Méchant", "Vintage", "Vierge", "Vision", "Volontaire", "Vulnérable", "Gaufre", "Recherché", "Richesse", "Armes", "Météo", "Mariage", "Femme", "Hiver", "Retiré", "Spirituel", "Coupe du monde", "Inquiet", "Vain", "Première Guerre mondiale", "Seconde Guerre mondiale", "Nocturne", "Somnambule", "Funambule", "Trapèze", "Cirque", "Spectacle", "Cage", "Compartiment", "Condiments", "Abonnement", "Quart", "Frange", "Cuir", "Bottes", "Soldes", "Slam", "Compétition", "Poésie", "Avancée", "Spectaculaire", "Admirable", "Amiral", "Compost", "Biologique", "Hamster", "Fouine"],
      emotion: ["abandonné", "abasourdi", "abattu", "absorbé", "accablé", "admiratif", "affectueux", "affolé", "agacé", "agité", "agonisant", "agressif", "aigri", "aimable", "aimant", "alarmé", "ambivalent", "amoureux", "amusé", "anéanti", "angoissé", "antipathique", "anxieux", "apathique", "arrogant", "attendri", "atterré", "attristé", "béatitude", "bégaiement", "bizarre", "blessé", "bouleversé", "braqué", "brisé", "cafard", "défensif", "dégonflé", "dégoût", "délaissé", "démoralisé", "déprimé", "dérangé", "dérouté", "désabusé", "désappointé", "désespéré", "désolé", "détendu", "désœuvré", "déstabilisé", "dévalorisé", "diminué", "distant", "doute", "doux", "drôle", "dubitatif", "dynamique", "ébahi", "éberlué", "ébloui", "écœuré", "effacé", "effarouché", "effrayé", "égaré", "égoïste", "embarrassé", "émerveillé", "empressé", "ému", "enchanté", "énergique", "énervé", "enflammé", "enivré", "enjoué", "enragé", "enthousiaste", "envieux", "envouté", "époustouflé", "épouvanté", "épuisé", "éreinté", "espiègle", "estomaqué", "étonné", "euphorique", "exalté", "exaspéré", "excité", "expansif", "exubérant", "fâché", "faible", "fasciné", "fatigué", "fier", "fou", "fougueux", "fragile", "franc", "frénétique", "frileux", "fringant", "frustré", "furieux", "gai", "galvanisé", "gêné", "gentil", "grincheux", "haineux", "hébété", "hésitant", "heureux", "honteux", "hostilité", "impatient", "imperturbable", "impressionné", "impulsif", "incertain", "indécis", "indigné", "inflexible", "inhibé", "inquiet", "insoumis", "insouciant", "inspiré", "intéressé", "interloqué", "intimidé", "intransigeant", "intrigué", "introverti", "irrité", "jaloux", "joyeux", "larmoyant", "las", "léger", "léthargique", "mal à l'aise", "malchanceux", "malheureux", "mécontent", "médusé", "méfiant", "mélancolique", "misérable", "momifié", "morose", "mortifié", "nerveux", "nonchalant", "nostalgique", "offensé", "obnubilé", "oppressé", "optimiste", "outragé", "outré", "paisible", "paniqué", "paralysé", "paresseux", "passionné", "peiné", "perplexe","pessimiste", "pétrifié", "peureux", "placide", "positif", "prudent", "rabaissé", "reconnaissance", "réjoui", "remuant", "répugnant", "résigné", "rêveur", "revigoré", "révolté", "ridiculisé", "romantique", "romanesque", "sensible", "sensuel", "somnolent", "soucieux", "souffrant", "sous-pression", "stressé", "submergé", "supérieur", "surpris", "suspicieux",  "sympathique", "tendre", "tendu", "terrifié", "timide", "tiraillé", "touché", "tracassé", "tranquille", "triomphant", "triste", "troublé", "turbulent", "vengeur", "veule", "vigoureux", "vindicatif", "voluptueux", "vulnérable"]
	};


var params = [
  {param:"currentpage",val:"welcome"},
  {param:"currentbg",val:1},
  {param:"currentbonus",val:1},
  {param:"theme",val:''},
  {param:"timer",val:0},
];

var names = [
  {name:"SYLVIA",style:"one",file:'SylviaRouge.png'},
  {name:"CARINE",style:"two",file:'CarineBlanc.png'},
  {name:"THOMAS",style:"three",file:'ThomasJaune.png'},
  {name:"ANTOINE",style:"four",file:'AntoineNoir.png'},
];

var background = [
  {name:"CHANTIER"},
  {name:"PLUIE"},
  {name:"GEEK"},
  {name:"VILLE"},
  {name:"POISSONS"},
  {name:"BLADE RUNNER"},
  {name:"TOMBES"},
  {name:"METRO"},
  {name:"LABO"},
  {name:"CHUTES"},
];

var bonus = [
  {id:1, name:"SILENCE",level:1,delay:45,desc:"Le comédien est muet pendant 45sec"},
  {id:2, name:"COLLE",level:1,delay:45,desc:"Le comédien a les pieds collés au sol pendant 45sec"},
  // {id:3, name:"AVEUGLE",level:1,delay:30,desc:"le comédien ferme les yeux pendant 30sec"},
  {id:4, name:"EMOTION",level:1,delay:30,desc:"le comédien doit jouer l'émotion donnée aléatoirement"},
  {id:5, name:"REMPLACEMENT",level:1,delay:45,desc:"Le comédien hors scène remplace un comédien en jeu pendant 45sec"},
  {id:6, name:"BOUGEOTE",level:1,delay:30,desc:"le comédien se déplace pendant 30sec"},

  // {id:7, name:"BOOMERANG",level:2,delay:5,desc:"Le bonus est renvoyé au comédien"},
  {id:8, name:"RIME",level:1,delay:45,desc:"Le comédien doit parler en rimes pendant 45sec"},
  {id:9, name:"STOP",level:1,delay:60,desc:"Le lanceur a le pouvoir de faire changer les propositions du comédien. fin du bonus 60sec"},
  {id:10, name:"ELLIPSE",level:1,delay:30,desc:"Avancer dans l'histoire"},
  // {id:11, name:"RIP",level:2,delay:20,desc:"Fin de la scène dans les 20 sec"},
  // {id:12, name:"CANNE À PÊCHE",level:2,delay:5,desc:"Le comédien vole le sort d'un autre"},
  {id:13, name:"TORNADE",level:1,delay:30,desc:"Les comédiens échangent leurs personnages"},
  // {id:14, name:"GROMELOT",level:2,delay:30,desc:"Le comédien parle avec une langue imaginaire pendant 30sec"},

  {id:15, name:"CHANT",level:1,delay:60,desc:"Le comédien chante à la place de parler pendant 60sec"},
  {id:16, name:"VOYAGE",level:1,delay:5,desc:"L'improvisation change de lieu"},
  {id:17, name:"SOLO",level:1,delay:45,desc:"Monologue du comédien pendant 45sec"},
  {id:18, name:"PALETTE",level:1,delay:30,desc:"Modifie la couleur de la scène/le type"},
  {id:19, name:"JUMEAUX",level:1,delay:45,desc:"Le comédien joue un 2eme personnage"},
  {id:20, name:"POISON",level:1,delay:30,desc:"Le comédien devra mourir dans les 30sec"},
  {id:21, name:"MUSICALE",level:1,delay:45,desc:"La scène est muette avec inspiration musicale pendant 45sec"}
];

var discoin = 50;

var soundsocket = undefined;

Meteor.methods( {
  addcoin: function (id,s) {
    Players.update( {id:id}, {$inc: {coins: s}});
  },
  newcoin:function () {
    //Streamy.broadcast('newcoin', { n: discoin });
    var nbc = parseInt(discoin*2*4 / Coins.find({}).count());
    Coins.update({},{$inc:{amount:nbc}}, {multi:true} );
  },
  newbonus:function () {
    //Streamy.broadcast('newcoin', { n: discoin });
    Players.update({},{$set:{coins:0}},{multi:true});

    var bonus = Bonus.find().fetch();
    var randomIndex = Math.floor( Math.random() * bonus.length );
    var b = bonus[randomIndex];

    // var nb = Math.floor( Math.random() * bonus.length );
    // var b = Bonus.findOne({id:nb});
    delete b._id;

    var t = 10;
    var id = Meteor.setInterval(function() {
      Params.update({param:'timer'},{$set:{val:t}});
      if (t==0) {
        Meteor.clearInterval(id);

        var p = Players.find({},{sort:{coins:-1},limit:1}).fetch();
        console.log('BONUS TO: '+p[0].id)
        PlayerBonus.insert({
          idp:p[0].id,
          id:b.id,
          name:b.name,
          delay:b.delay,
          level:b.level
        })
      }

      t--;
    },1000);

    Params.update({param:'currentbonus'},{$set:{val:b}});

    Streamy.broadcast('newbonus');
  },
  // addbonus:function (idp,idb) {
  //   var b = Bonus.findOne({id:parseInt(idb)});
  //   var c = Players.findOne({id:idp}).coins;
  //   if (c>=5*discoin*b.level) {
  //     PlayerBonus.insert({
  //       idp:idp,
  //       id:idb,
  //       name:b.name,
  //       delay:b.delay,
  //       level:b.level
  //     })
  //     // idpf gives coins for bonus
  //     Streamy.broadcast('bonusstart');
  //
  //     var dec = 5*discoin*b.level;
  //     var id = Meteor.setInterval( function() {
  //       Players.update({id:idp}, {$inc: { coins: -1}});
  //       if (dec--<0)
  //       Meteor.clearInterval(id);
  //     }, 50);
  //
  //     //Players.update({id:idp}, {$inc: { coins: -5*discoin*b.level}});
  //   }
  // },
  buybonus:function (idp,l) {
    var array = Bonus.find({level:l}).fetch();
    var randomIndex = Math.floor( Math.random() * array.length );
    var b = array[randomIndex];

    var c = Players.findOne({id:idp}).coins;
    if (c>=discoin*b.level) {
      PlayerBonus.insert({
        idp:idp,
        id:b.id,
        name:b.name,
        delay:b.delay,
        level:b.level
      })
      // idpf gives coins for bonus

      Players.update({id:idp}, {$inc: { coins: -discoin*b.level}});
    }
  },
  randomtheme: function(cl) {
    var t = suggestions[cl][Math.floor((Math.random()*suggestions[cl].length))];
    Params.update({param:'theme'},{$set:{val:cl+' : '+t}});
  },
  resettheme: function() {
    Params.update({param:'theme'},{$set:{val:''}});
  },
  sendbonus:function(idpf,idb,idpt) {
    console.log("bonus f:"+idpf+" "+idpt+" : "+idb)
    var rb = PlayerBonus.findOne({
      idp:parseInt(idpf),
      id:parseInt(idb),
    })
    if (rb)
    PlayerBonus.remove({
      _id:rb._id
    });
    // send bonus to player idpt
    var b = Bonus.findOne({id:parseInt(idb)});
    if (b) {
      Streamy.emit('bonusstart',{},soundsocket);

      if (idb==4) {//emotion
        var t = suggestions['emotion'][Math.floor((Math.random()*suggestions['emotion'].length))];
        Params.update({param:'theme'},{$set:{val:'emotion : '+t}});
      }

      if ((idb==5)||(idb==13)) { // échange comédiens...
        PlayerBonusActive.insert({
          idp:parseInt(idpf),
          id:parseInt(idb),
          name:b.name,
          desc:b.desc
        });

        // will remove it in XX delay
        Meteor.setTimeout(function() {
          PlayerBonusActive.remove({
            idp:parseInt(idpf),
            id:parseInt(idb)
          });
        }, b.delay*1000);
      }

      if (idb==21) {
        var ms = Musics.find({played:false}).fetch();
        console.log(ms);
        const m = ms[Math.floor((Math.random()*ms.length))];
        Musics.update({id:m.id},{$set:{played:true}});
        console.log("PLAY:"+m.id);
        Streamy.emit('musicgo', {file:'Musique'+m.id+'.mp3'},soundsocket);
      }

      PlayerBonusActive.insert({
        idp:parseInt(idpt),
        id:parseInt(idb),
        name:b.name,
        desc:b.desc
      });

      // will remove it in XX delay
      Meteor.setTimeout(function() {
        PlayerBonusActive.remove({
          idp:parseInt(idpt),
          id:parseInt(idb)
        });
        Streamy.emit('bonusend',{},soundsocket);
      }, b.delay*1000);
    }
  },
  showplayer:function(idp) {
    Players.update({id:idp},{$set:{visible:true}});
  },
  changebg:function(id) {
    Params.update({param:'currentbg'},{$set:{val:id}});
    // Background.update({id:0},{$set:{file:f}});
  },
  newclient:function(aid) {
    console.log("NEW CLIENT : "+aid);
    const c = Coins.findOne({id:aid});
    if (!c) {
      Coins.insert({
        id:aid,
        amount:0
      })
      console.log("CREATED "+aid)
    }

  },
  setamount:function(c,a) {
    Coins.update({id:c},{$set:{amount:a}});
    console.log("nb users:"+Coins.find({}).count());
  }
  // hide: function() {
  //   Players.update( {}, {$set: {hidden:true}} , {multi:true}  )
  // },
  // show: function() {
  //   Players.update( {}, {$set: {hidden:false}} , {multi:true}  )
  // },
})

Meteor.startup(() => {

  Streamy.on('soundbox', function(data, from) {
    soundsocket = from;
  });

    Streamy.BroadCasts.allow = function(data, from) {
      return true;
    };

    Players.remove({})
    Bonus.remove({})
    PlayerBonus.remove({});
    PlayerBonusActive.remove({});
    Background.remove({});
    Params.remove({});
    Coins.remove({});
    Musics.remove({});

    if (Players.find().count() === 0)
    {
      var id=1
      _.each(names, function (p) {
        Players.insert({
          id: id++ ,
          name: p.name,
          class : p.style,
          file: p.file,
          coins: 0,
          visible:false
        })
      })
    }


    if (Bonus.find().count() === 0)
    {
      //var id=1
      _.each(bonus, function (b) {
        Bonus.insert({
          id: b.id ,
          name: b.name,
          level: b.level,
          delay: b.delay,
          desc:b.desc
        })
      })
    }

    if (Background.find().count() === 0)
    {
      var id=1;
      _.each(background, function (b) {
        Background.insert({
          id: id ,
          name: b.name,
          file: 'bg'+id+".gif",
        });
        id++;
      })
    }

    if (Params.find().count() === 0)
    {
      _.each(params, function (p) {
        Params.insert({
          param: p.param,
          val: p.val,
        })
      })
    }

    if (Musics.find().count() === 0)
    {
      for (var id=1;id<=14;id++){
        Musics.insert({
          id: id,
          played: false,
        })
      };
    }


});
