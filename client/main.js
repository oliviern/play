import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { FlowRouter } from 'meteor/kadira:flow-router'
//import {ClientStorage} from 'ClientStorage';
import { Session } from 'meteor/session';
import { Random } from 'meteor/random';
import { Cookies } from 'meteor/ostrio:cookies';

const cookies = new Cookies();

Players = new Mongo.Collection("players")
Bonus = new Mongo.Collection("bonus")
PlayerBonus = new Mongo.Collection("playerbonus")
PlayerBonusActive = new Mongo.Collection("playerbonusactive")
Background = new Mongo.Collection("background");
Params = new Mongo.Collection("params");
Coins = new Mongo.Collection("coins");

import './main.html';

// Streamy.on('newcoin', function(data) {
//   new Audio('/sounds/coin.mp3').play();
//   var c = Session.get('coins') || 0;
//   Session.set('coins', c+data.n)
// });

var anon_id = undefined;

// function getAmount() {
//   if (!anon_id) return 0;
//   const coins = Coins.findOne({id:anon_id});
//   if (!coins) return 0;
//   return coins.amount;
// }

function setAmount(a) {
  if (!anon_id) return;
  Meteor.call('setamount', anon_id,a);
}

Template.display.helpers({
    players: function () {
      return Players.find({visible:true})
    },
    theme: function() {
      var t = Params.findOne({param:'theme'});

      if (!t) return '';

      if (t.val=='')
        $('.theme').fadeOut(0);
      else
        $('.theme').fadeIn(1000);

      return t.val;
    },
    timer : function() {
      var t = Params.findOne({param:'timer'});
      if (t==undefined) return 0;
      return t.val;
    },
    timerok : function() {
      var t = Params.findOne({param:'timer'});
      if (t==undefined) return false;
      return (t.val>0);
    }
});
Template.player_display.helpers({
    playerbonus: function (id) {
      return PlayerBonus.find({idp:id},{sort:{id:1}});
    },
    playerbonusactive: function (id) {
      return PlayerBonusActive.find({idp:id},{sort:{id:1}});
    },
    playerbonusactivedesc: function (id) {
      return PlayerBonusActive.find({idp:id},{sort: {_id: -1}});
    },
    cbg:function() {
      var cbg = Params.findOne({param:'currentbg'});
      return Background.findOne({id:cbg.val}).file;
    },
    timer : function() {
      var t = Params.findOne({param:'timer'});
      if (t==undefined) return 0;
      return t.val;
    },
    timerok : function() {
      var t = Params.findOne({param:'timer'});
      if (t==undefined) return false;
      return (t.val>0);
    }
});

Template.mobile.helpers({
    currentpage : function() {
      var p = Params.findOne({param:'currentpage'});
      if (p==undefined) return 'welcome';
      return p.val;
    }
});
Template.welcome.helpers({
  bonus: function () {
    return Bonus.find({})
  },
});
Template.public.helpers({
    players: function () {
      return Players.find()
    },
    mycoins : function() {
      const anon_id = cookies.get("anon_id");
      const coins = Coins.findOne({id:anon_id});

      if (!coins) return 0;
      return coins.amount;
      //return Session.get('coins');
    },
    newbonus : function() {
      var b = Params.findOne({param:'currentbonus'});
      return b.val;
    },
    timer : function() {
      var t = Params.findOne({param:'timer'});
      if (t==undefined) return 0;
      return t.val;
    },
    timerok : function() {
      var t = Params.findOne({param:'timer'});
      if (t==undefined) return false;
      return (t.val>0);
    }
});
Template.player_public.helpers({
  playerbonus: function (id) {
    return PlayerBonus.find({idp:id},{sort:{id:1}});
  },
  timer : function() {
    var t = Params.findOne({param:'timer'});
    if (t==undefined) return 0;
    return t.val;
  },
  timerok : function() {
    var t = Params.findOne({param:'timer'});
    if (t==undefined) return false;
    return (t.val>0);
  }

});

Template.public.onCreated(function loginOnCreated() {

  anon_id = cookies.get("anon_id");
  if (!anon_id) {
    anon_id = Random.id();
    cookies.set("anon_id", anon_id);
  }

  // create account if unknown
  Meteor.call('newclient', anon_id);

});

var lastid=0;
Template.public.events({
  "click .player": function(event, template){

    var t = Params.findOne({param:'timer'});
    if (t.val==0) return;

    if (lastid==this.id) return;

    $(".player").removeClass('playersel');
    switch (this.id) {
      case 1: $(".one").addClass('playersel'); break;
      case 2: $(".two").addClass('playersel'); break;
      case 3: $(".three").addClass('playersel'); break;
      case 4: $(".four").addClass('playersel'); break;
    }

    if (lastid>0)
      Meteor.call('addcoin', lastid, -1 );

    Meteor.call('addcoin', this.id, 1 );
    lastid=this.id;
    // var c = getAmount();
    // if (c>0) {
    //   new Audio('/sounds/coin.mp3').play();
    //   Meteor.call('addcoin', this.id, 1 );
    //   setAmount(c-1);
    // }
  }
});

const temp = Template.instance()

Streamy.on('newbonus', function(data) {
  console.log('SOCKET NEWBONUS');
  this.$(".player").removeClass('playersel');
  lastid=0;
});


// REGIE
Streamy.on('bonusstart', function(data) {
    new Audio('/sounds/bonusstart.wav').play();
});

Streamy.on('bonusend', function(data) {
    new Audio('/sounds/bonusend.wav').play();
});

Streamy.on('musicgo', function(data) {
    //console.log (Template.instance().view.name);
    var audio = new Audio('/musics/'+data.file);
    audio.play();
    Meteor.setTimeout( function() {
      console.log("STOP MUSIC");
      audio.pause();
    },45000);
});

Template.regie.onCreated(function() {
  Streamy.onConnect(function(socket) {
    Streamy.emit('soundbox');
  });
})

Template.regie.helpers({
    players: function () {
      return Players.find()
    },
    backgrounds : function() {
      return Background.find()
    },
    timer : function() {
      var t = Params.findOne({param:'timer'});
      if (t==undefined) return 0;
      return t.val;
    },
    timerok : function() {
      var t = Params.findOne({param:'timer'});
      if (t==undefined) return false;
      return (t.val>0);
    }
});
Template.regie.events({
  "click .bonus": function(event, template){
      Meteor.call(
        'addbonus',
        parseInt($(event.target).attr('idp')),
        parseInt(this.id) );
  },
  "click #idthemelieu": function(event,template){
    var tot = 20;
    var id = Meteor.setInterval( function() {
      Meteor.call("randomtheme","lieu")
      if (tot--<0)
      Meteor.clearInterval(id);
    }, 150)
  },
  "click #idthemerelation": function(event,template){
    var tot = 20;
    var id = Meteor.setInterval( function() {
      Meteor.call("randomtheme","relation")
      if (tot--<0)
      Meteor.clearInterval(id);
    }, 150)
  },
  "click #idthememot": function(event,template){
    var tot = 20;
    var id = Meteor.setInterval( function() {
      Meteor.call("randomtheme","mot")
      if (tot--<0)
      Meteor.clearInterval(id);
    }, 150)
  },
  "click #idthemeemotion": function(event,template){
    var tot = 20;
    var id = Meteor.setInterval( function() {
      Meteor.call("randomtheme","emotion")
      if (tot--<0)
      Meteor.clearInterval(id);
    }, 150)
  },
  "click #idthemereset": function(event,template){
    Meteor.call(
      "resettheme"
    )
  },
  "click #idcoin5": function(event, template){
    // add coin to all public
    Meteor.call('newcoin' )
  },
  "click #idbonus": function(event, template){
    // add coin to all public
    Meteor.call('newbonus' )
  },
  "click #idsend": function(event, template){
    var idpf = parseInt($(event.target).attr('idpf'));
    var idb = parseInt($('#bs'+idpf).val());
    var idpt = parseInt($('#ps'+idpf).val());
    Meteor.call('sendbonus',idpf,idb,idpt );
  },
  "click #idshow": function(event, template){
    var idp = parseInt($(event.target).attr('idp'));
    Meteor.call('showplayer',idp );
  },
  "change .bonus-select": function(event, tamplate){
    var idpf = parseInt($(event.target).attr('idpf'));
    var timer = $('#bs'+idpf).find(":selected").attr('timer');
    $('#idt'+idpf).val(timer);
  },
  "change .background-select": function(event,template){
    var id = parseInt($('.background-select').val());
    Meteor.call('changebg',id );
  },
  "change .page-select": function(event,template){
    var cp = $('.page-select').val();
    var p = Params.findOne({param:'currentpage'});
    Params.update({_id:p._id},{$set:{val:cp}});
  },
  "click #idaddcoin": function(event, template){
      Meteor.call('addcoin', this.id, 100 );
  },
  "click #idbuy1": function(event, template){
    Meteor.call('buybonus',parseInt($(event.target).attr('idp')),1 );
  },
  "click #idbuy2": function(event, template){
    Meteor.call('buybonus',parseInt($(event.target).attr('idp')),2 );
  },
  "click #idbuy3": function(event, template){
    Meteor.call('buybonus',parseInt($(event.target).attr('idp')),3 );
  }
});
Template.player_regie.helpers({
    bonus: function () {
      return Bonus.find({})
    },
    players: function () {
      return Players.find({})
    },
    playerbonus: function (id) {
      return PlayerBonus.find({idp:id},{sort:{id:1}});
    },
});

FlowRouter.route('/', {
  //name: 'scenes',
  action: function(params, queryParams) {
    BlazeLayout.render('mobile', {main: 'scores'});
  }
});

FlowRouter.route('/display', {
  //name: 'admin',
  action: function(params, queryParams) {
  BlazeLayout.render('display', {main: 'admin'});
  }
});

FlowRouter.route('/regie', {
  //name: 'admin',
  action: function(params, queryParams) {
  BlazeLayout.render('regie', {main: 'admin'});
  }
});

 // Template.scores.uihooks({
 //  '.bonusactive': {
 //    container: '.bonusactive',
 //    insert: function(node, next) {
 //      $(node).css({zoom:0.1,opacity:0});
 //      $(node).insertBefore(next)
 //      $(node).animate( {zoom:1,opacity:1},1000, function() {
 //      })
 //    },
 //    move: function(node, next) {
 //      $(node).animate( {zoom:0.1,opacity:0}, 1000,  function(){
 //    	  $(node).insertBefore(next);
 //    	  $(node).animate( {zoom:1,opacity:1},1000, function() {
 //    		    $(node).removeAttr( 'style' );
 //  	    });
 //      });
 //    },
 //    remove: function(node) {
 //      $(node).animate(  {zoom:0.1,opacity:0}, 1000,  function(){
 //        $(node).remove();
 //      });
 //    }
 //  }
 //  });
